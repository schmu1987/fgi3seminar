\select@language {german}
\contentsline {section}{\numberline {1}Einleitung}{2}
\contentsline {subsection}{\numberline {1.1}Motivation}{2}
\contentsline {subsection}{\numberline {1.2}Probleme und Fragen}{2}
\contentsline {subsection}{\numberline {1.3}Die Theorie der Iteration von Ensembles}{2}
\contentsline {subsection}{\numberline {1.4}Aufbau der Arbeit}{2}
\contentsline {section}{\numberline {2}Infinite Games}{2}
\contentsline {subsection}{\numberline {2.1}Arena}{2}
\contentsline {subsection}{\numberline {2.2}Spielz\"uge}{2}
\contentsline {subsection}{\numberline {2.3}Spiele und wie wir gewinnen}{2}
\contentsline {subsection}{\numberline {2.4}Gewinnbedingungen}{3}
\contentsline {subsection}{\numberline {2.5}Strategien und Determinismus}{3}
\contentsline {subsection}{\numberline {2.6}Transformation von Gewinnbedingungen}{4}
\contentsline {subsection}{\numberline {2.7}Determinismus}{4}
\contentsline {subsection}{\numberline {2.8}Vergessende und Speicherlose Strategien}{4}
\contentsline {section}{\numberline {3}L\"osen von Spielen mit simplen Gewinnbedingungen}{5}
\contentsline {subsection}{\numberline {3.1}Erreichbarkeitsspiele}{5}
\contentsline {subsection}{\numberline {3.2}B\"uchi-Akzeptanz}{6}
\contentsline {section}{\numberline {4}Ausblick und Zusammenfassung}{6}
\contentsline {subsection}{\numberline {4.1}Zusammenfassung}{6}
\contentsline {subsection}{\numberline {4.2}Ausblick}{6}
