\beamer@endinputifotherversion {3.36pt}
\beamer@sectionintoc {1}{Einf\"uhrung}{3}{0}{1}
\beamer@sectionintoc {2}{Definitionen}{9}{0}{2}
\beamer@subsectionintoc {2}{1}{Arena}{10}{0}{2}
\beamer@subsectionintoc {2}{2}{Partie}{15}{0}{2}
\beamer@subsectionintoc {2}{3}{Gewinnen}{21}{0}{2}
\beamer@sectionintoc {3}{Gewinnbedingungen}{27}{0}{3}
\beamer@sectionintoc {4}{Strategien}{38}{0}{4}
\beamer@sectionintoc {5}{Konklusion}{51}{0}{5}
